﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SignalR.Hubs;

namespace EchoService.Hubs
{
    public class EchoHub : Hub
    {
        public void SendMessage(string message)
        {
            Clients.Echo(message);
        }
    }
}