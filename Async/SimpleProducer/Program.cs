﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace SimpleProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory();

            using(var connection = factory.CreateConnection())
            {
                var channel = connection.CreateModel();
                channel.ExchangeDeclare("SA", ExchangeType.Direct);

                while(true)
                {
                    var code = Console.ReadKey();
                    if (code.KeyChar == 'q')
                        return;

                    Console.WriteLine("Publish message");
                    channel.BasicPublish("SA", "SA", null, Encoding.UTF8.GetBytes("Hello World"));
                }
            }
        }
    }
}
