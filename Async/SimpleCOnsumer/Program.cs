﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace SimpleConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                var factory = new ConnectionFactory();

                using (var connection = factory.CreateConnection())
                {
                    var channel = connection.CreateModel();
                    channel.ExchangeDeclare("SA", ExchangeType.Direct);

                    string name = channel.QueueDeclare();                    
                    var consumer = new QueueingBasicConsumer(channel);

                    channel.QueueBind(name, "SA", "SA");
                    channel.BasicConsume(name, true, consumer);

                    while (true)
                    {
                        var message = (BasicDeliverEventArgs)consumer.Queue.Dequeue();

                        Console.WriteLine(Encoding.UTF8.GetString(message.Body));
                    }
                }
            }
        }
    }
}
