﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Entities;
using InMemoryRepository;
using Moq;
using NUnit.Framework;
using ToDo.Controllers;
using ToDo.Models;

namespace WebSiteTests
{
    [TestFixture]
    public class ToDoControllerTests
    {
        private Mock<IToDoRepository> _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<IToDoRepository>();
        }

        [Test]
        public void GivenAToDoController_WhenTheIndexMethodIsCalled_ThenTheCorrectViewIsReturned()
        {
            var controller = new TodoController(_repository.Object);
            var result = controller.Index();

            Assert.That(result, Is.TypeOf<ViewResult>());
        }

        [Test]
        public void GivenFourTodoItemsInAList_WhenTheIndexMethodIsCalled_ThenFourViewModelsAreReturned()
        {
            _repository.Setup(r => r.Entities()).Returns(
                new List<Todo> { new Todo(), new Todo(), new Todo(), new Todo() });
            var controller = new TodoController(_repository.Object);
            var result = controller.Index() as ViewResult;
            var model = result.Model as IEnumerable<TodoViewModel>;

            Assert.That(model.Count(), Is.EqualTo(4));
        }

        [Test]
        public void GivenInvalidData_WhenTheTodoIsCreated_ThenTheControllerRedisplaysTheNewPage()
        {
            var controller = new TodoController(_repository.Object);
            controller.ModelState.AddModelError("key", "value");
            ActionResult result = controller.Create(new TodoViewModel());

            Assert.That(result, Is.TypeOf<ViewResult>());
        }

        [Test]
        public void GivenValidData_WhenTheTodoIsCreated_ThenTheControllerDisplaysAllTheTodos()
        {
            var controller = new TodoController(_repository.Object);
            ActionResult result = controller.Create(new TodoViewModel());

            Assert.That(result, Is.TypeOf<RedirectToRouteResult>());

        }
    }
}
