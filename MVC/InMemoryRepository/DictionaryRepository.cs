﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace InMemoryRepository
{
    public class ToDoCollection : IToDoRepository
    {
        static readonly Dictionary<int, Todo> Todos = new Dictionary<int, Todo>();

        static ToDoCollection()
        {
            Todos.Add(1, new Todo { Id = 1, Priority = 1, Description = "Finish Repository Code", CompletionDate = DateTime.Now });
            Todos.Add(2, new Todo { Id = 2, Priority = 1, Description = "Have a coffee", CompletionDate = DateTime.Now });
            Todos.Add(3, new Todo { Id = 3, Priority = 1, Description = "Go on holiday!!!!", CompletionDate = DateTime.Now });
        }

        public IEnumerable<Todo> Entities()
        {
            return Todos.Values;
        }

        public void Create(Todo entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Todo entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Todo entity)
        {
            throw new NotImplementedException();
        }
    }
}
