using Entities;
using Repository;

namespace InMemoryRepository
{
    public interface IToDoRepository : IRepository<Todo>
    {
    }
}