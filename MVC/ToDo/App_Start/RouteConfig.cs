﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ToDo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "todo-new",
                url: "todo/new",
                defaults: new { controller = "Todo", action = "New" },
                constraints: new { httpMethod = new HttpMethodConstraint("GET") }
            );
            routes.MapRoute(
                name: "todo-create",
                url: "todo/create",
                defaults: new { controller = "Todo", action = "Create" },
                constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            );
            routes.MapRoute(
                name: "todo-index",
                url: "todo",
                defaults: new { controller = "Todo", action = "Index" },
                constraints: new { httpMethod = new HttpMethodConstraint("GET") }
            );

            routes.MapRoute(
                name: "profile",
                url: "profiles/{name}",
                defaults: new { controller = "Profile", action = "Index" },
                constraints: new { httpMethod = new HttpMethodConstraint("GET") }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Todo", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}