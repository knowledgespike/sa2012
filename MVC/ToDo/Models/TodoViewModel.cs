using System;
using System.ComponentModel.DataAnnotations;

namespace ToDo.Models
{
    public class TodoViewModel 
    {
        [Required(ErrorMessage = "Say What!")]
        public string What   { get; set; }
        [Range(1, 5)]
        public int Priority { get; set; }
        [UIHint("Date")]
        public DateTime When { get; set; }
    }
}