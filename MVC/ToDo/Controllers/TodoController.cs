﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities;
using InMemoryRepository;
using ToDo.Models;

namespace ToDo.Controllers
{
    public class TodoController : Controller
    {
        readonly IToDoRepository _repository;

        public TodoController(IToDoRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            var models = _repository.Entities().Select(entity => new TodoViewModel { What = entity.Description, When = entity.CompletionDate, Priority = entity.Priority }).ToList();

            // populate

            return View(models);
        }

        public ActionResult New()
        {
            return View(new TodoViewModel{When = DateTime.Now});
        }

        public ActionResult Create(TodoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("New", model);
            }
            //_repository.Create(new Todo());
            return RedirectToAction("Index");
        }
    }
}
